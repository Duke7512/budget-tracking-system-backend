const mongoose = require('mongoose');

const IncomeCategorySchema = new mongoose.Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        reqiure: true
    },
    category: {
        type: String,
        require: true
    },
});

module.exports = mongoose.model('IncomeCategory', IncomeCategorySchema);
