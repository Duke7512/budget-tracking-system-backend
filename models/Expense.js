const mongoose = require('mongoose');

const ExpenseSchema = new mongoose.Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        reqiure: true
    },
    expenseCategoryId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "ExpenseCategory",
        reqiure: true
    },
    amount: {
        type: Number,
        require: true
    },
});

module.exports = mongoose.model('Expense', ExpenseSchema);
