const mongoose = require('mongoose');

const ExpenseCategorySchema = new mongoose.Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        reqiure: true
    },
    category: {
        type: String,
        require: true
    },
});

module.exports = mongoose.model('ExpenseCategory', ExpenseCategorySchema);
