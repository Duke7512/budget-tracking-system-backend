const router = require('express').Router();
const { verifyUser } = require('./../auth.js');
const { 
		getAllIncome, 
		addIncome, 
		updateIncome, 
		deleteIncome, 
		getAllExpense, 
		addExpense, 
		updateExpense, 
		deleteExpense 
} = require('./../controllers/transactionController');
const { 
		addIncomeCategory, 
		getAllIncomeCategory, 
		updateIncomeCategory, 
		deleteIncomeCategory, 
		getAllExpenseCategory, 
		addExpenseCategory, 
		updateExpenseCategory, 
		deleteExpenseCategory 
} = require('./../controllers/categoryController');

router.get('/income', verifyUser, getAllIncome);
router.post('/:id/income', verifyUser, addIncome);
router.put('/:id/income/:incomeId', verifyUser, updateIncome);
router.delete('/:id/income/:incomeId', verifyUser, deleteIncome);

router.get('/expense', verifyUser, getAllExpense);
router.post('/:id/expense', verifyUser, addExpense);
router.put('/:id/expense/:expenseId', verifyUser, updateExpense);
router.delete('/:id/expense/:expenseId', verifyUser, deleteExpense);

router.get('/incomeCategory', verifyUser, getAllIncomeCategory);
router.post('/incomeCategory', verifyUser, addIncomeCategory);
router.put('/incomeCategory/:id', verifyUser, updateIncomeCategory);
router.delete('/incomeCategory/:id', verifyUser, deleteIncomeCategory);

router.get('/expenseCategory', verifyUser, getAllExpenseCategory);
router.post('/expenseCategory', verifyUser, addExpenseCategory);
router.put('/expenseCategory/:id', verifyUser, updateExpenseCategory);
router.delete('/expenseCategory/:id', verifyUser, deleteExpenseCategory);

module.exports = router;
