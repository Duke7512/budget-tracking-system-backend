const router = require('express').Router();
const { verifyUser } = require('./../auth.js');
const { register, login, getUsers } = require('./../controllers/userController')

router.post('/register', register);
router.post('/login', login);
router.get('/', verifyUser, getUsers);

module.exports = router;
