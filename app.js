const express = require('express');
const cors = require('cors')

require('dotenv').config();
require('./db');

const app = express();
const port = process.env.PORT || 4000;

// middleware
app.use(cors())
app.use(express.json());

// routes
app.use('/api/users', require('./routes/userRoutes'));
app.use('/api/transactions', require('./routes/transactionRoutes.js'));

app.listen(port, () => {
    console.log(`Server running on port ${port}`);
});
